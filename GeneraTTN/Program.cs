﻿using System;
using ITools;
using ITTN_MySQL;
using System.Runtime.InteropServices;
using System.Text;
using IConexionBD;
using System.Data;

namespace GeneraTTN
{
    public static class  Program
    {
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("Kernel32")]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("Kernel32.dll")]
        static extern bool AlloConsole();//Oculta la pantalla 

        static readonly ClsLog RegistraLog = new ClsLog();
        static readonly ClsBDComun ConBd = new ClsBDComun();
        static string strServ;

        static void Main(string[] args)
        {
            
            ClsTtn GeneraTTN = new ClsTtn();
            string strTTN;
            bool bolTipTTN;
            IntPtr hwnd;
            
            try
            {
                hwnd = GetConsoleWindow();
                ShowWindow(hwnd, 0);
               
                strServ = args[0].ToString();
                bolTipTTN = bool.Parse(args[1].ToString());
                
                if(bolTipTTN)
                {
                    strTTN = "_TransformaTablaNombre";
                }
                else
                {
                    strTTN = "_TransformaTablaNombreConsultas";
                }
                RegistraLog.EscribeLog("Inicia TTN");
                GeneraTTN.CreaTTN(strServ, "catalogocentralizado", "scat_ttn", "scat_ttn*", 3306, strTTN);
                RegistraLog.EscribeLog("Termina TTN");

                if(strTTN == "_TransformaTablaNombre")
                {
                    RegistraLog.EscribeLog("Inicia Cambios en TTN");
                    CambiosTTN();
                    RegistraLog.EscribeLog("Termina Cambios en TTN");

                    RegistraLog.EscribeLog("Inicia Federación TTN");
                    FederaTTN();
                    RegistraLog.EscribeLog("Termina Federación TTN");
                }
               
            }
            catch (Exception ex)
            {
                RegistraLog.EscribeLog(ex.ToString());
            }
        }

        static void FederaTTN()
        {
            StringBuilder strSql = new StringBuilder();
            DataSet dsCreate;
            string strFed;
            string strCad = "Server= Jaguar;Port=3306;Database= catalogocentralizado;Uid= si_momj; Pwd=si_momj; CharSet= utf8;";
            string strCadP = "Server= "+ strServ + ";Port=3306;Database= catalogocentralizado;Uid= si_momj; Pwd=si_momj; CharSet= utf8;";
            try
            {

                strSql.Clear();
                strSql.Append("DROP TABLE catalogocentralizado._TransformaTablaNombre;");
                ConBd.Execute(strSql.ToString(), strCad, 2);
                strSql.Clear();
                strSql.Append("SHOW CREATE TABLE catalogocentralizado._TransformaTablaNombre");
                dsCreate =ConBd.DataAdapter(strSql.ToString(), strCadP, 2);
                strFed = dsCreate.Tables[0].Rows[0][1].ToString();
               strFed =  strFed.Replace("ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin",
                                "ENGINE = FEDERATED DEFAULT CHARSET = utf8 CONNECTION = 'mysql://si:snib2018@puma:3306/catalogocentralizado/_TransformaTablaNombre'");
                ConBd.Execute(strFed, strCad, 2);
                
            }
            catch(Exception ex)
            {
                RegistraLog.EscribeLog(ex.ToString());
            }
        }

        static void CambiosTTN()
        {
            StringBuilder strSql = new StringBuilder();
            string strCadP = "Server= " + strServ + ";Port=3306;Database= catalogocentralizado;Uid= si_momj; Pwd=si_momj; CharSet= utf8;";
            try
            {
                strSql.Clear();
                strSql.Append("CALL catalogocentralizado.01_ActualizaCambiosEnCatalogocentralizado;");
                ConBd.Escalar(strSql.ToString(), strCadP, 2);
                strSql.Clear();
                strSql.Append("CALL catalogocentralizado.02_ActualizaInfoAdicional;");
                ConBd.Escalar(strSql.ToString(), strCadP, 2);
                strSql.Clear();
                strSql.Append("CALL catalogocentralizado.03_ActualizaNombreComun;");
                ConBd.Escalar(strSql.ToString(), strCadP, 2);
            }
            catch (Exception ex)
            {
                RegistraLog.EscribeLog(ex.ToString());
            }
        }
    }
}
